using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using standardfunctions;
using standardclasses;

namespace HighScoreTable
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        // graphics 2D class types for backgrounds
        graphic2d menuBackground;
        graphic2d gameOverImage;
        graphic2d youWin;
        graphic2d tutorial;
        graphic2d gameOverScreen;

        // Screen size
        int displayWidth;
        int displayHeight;

        int level = 0;         // current level
        float scrollValue = 0; // scroll value is used to check which state/point the mouse scroller is at for switching the pipe types

        // Time Variables 
        float gameStartTime = 0;
        float gameTimeGone = 0;
        const int lengthOfRound = 60; // how long each level timer is
        const int numberOfLevels = 6; // Number of levels in game


        // Screen Distance & Aspects
        float aspectRatio;                                              // 3d to 2d scaling ratio
        const int cameraDistance = 1130;                                // Distance from objects to camera
        Vector3 cameraPosition = new Vector3(0, 0, cameraDistance);     // the 3d camera's position
        Vector3 cameraLookAt = new Vector3(0, 0, 0);                    // what the camera should look at
        const float fov = 45;                                           // Angle of FOV for camera
     
        Vector2 cameraView;   // Limits to viewable area on screen
        float scaleFactor;    // used for positioning the spanner on the mouse, as mouse uses different co-ords
        int stick = -1;       // variable used for checking/setting if a pipe is currently picked up                                        
      
        // basic3DModel struct
        struct basic3DModel
        {
            public Model graphic;           // The 3D Model object that we are going to display.
            public Matrix[] transforms;     // Holds model transformation matrix
            public Vector3 rotation;        // Amount of rotation to apply on x,y and z
            public Vector3 position;        // Position on screen
            public float size;              // Size ratio 
            public float radius;            // Radius of 3D model
            public BoundingSphere bSphere;  // Bounding sphere
        }
        
        // struct for the pipes
        struct pipes
        {
            public Model graphic;           // The 3D Model object that we are going to display.
            public Matrix[] transforms;     // Holds model transformation matrix
            public Vector3 rotation;        // Amount of rotation to apply on x,y and z
            public Vector3 position;        // Position on screen
            public float size;              // Size ratio 
            public float radius;            // Radius of 3D model
            public BoundingSphere bSphere;  // Bounding sphere
            public int pipeType;            // current pipeType, changes model and pipe type(straight = 0, curved = 1)
        }

        int numberOfPipes;                   // Number of pipes
        pipes[,] pipe = new pipes[48, 2];    // Array of pipes 48 is the max Available Number of Pipes, and 2 is the different types.

        // struct for obstacles also used for puzzle pieces
        struct obstacles
        {
            public Model graphic;           // The 3D Model object that we are going to display.
            public Matrix[] transforms;     // Holds model transformation matrix
            public Vector3 position;        // Position on screen
            public float size;              // Size Ratio
            public float radius;            // Radius of 3D model
            public BoundingBox bBox;        // Bounding Box
            public Vector3 rotation;        // Amount of rotation to apply on x,y and z
            public float width;             // Width of Obstacle
            public BoundingSphere bSphere;  // Bounding Sphere
            public int value;               // give a numbered value for the Obstacle
        }



        const int numberOfPuzzlePieces = 48;                          // Number Of Puzzle Pieces 
        obstacles[,] puzzle = new obstacles[numberOfPuzzlePieces, 2]; // Array Of Puzzle Pieces, numberOf and a second set of them, 
                                                                      // reason for this is that we want a "correct set" and the players "current set" 
                                                                      // so we can match them up and see if they have the correct path/puzzle placement

        int numberOfObs;                     // Number of Obstacles
        obstacles[] obs = new obstacles[48]; // Array And Available Number of Obstacles, never actually use this many so...waste of resources need to change

        basic3DModel spanner;                      // initiating mouse model

        SpriteFont fontWhite, digitalFont;      // Fonts for use in game

        SoundEffect pipeDrop, gameOver, firstLevel, fourthLevel; //Initiating Sound
        SoundEffectInstance music1, music2;

        Boolean gameOverBool = false; // Initiating Booleans And setting them to false
        Boolean levelCompleteBool = false;
        Boolean keyReleased = true;             // Have keys been released
        Boolean mouseReleased = true;           // Has mouse been released

        Random randomiser = new Random();           // Variable to generate random numbers

        int menuSelection = 0;                  // Menu option selected
        int gameMode = 0;                       // What mode is game in (selected from main menu)
        int score = 0;                          // Score

        // sprite2d's are similar to graphic2d except that graphic2d's are more specific to backgrounds
        // stretched to the screen aspect ratio. Where as sprites have bbox's etc built in 
        const int numOfChoices = 4;               // Number of menu options
        sprite2d[,] menuChoices = new sprite2d[numOfChoices, 2]; // Array of menu options, NumberOf and current image (highlighted, unhighlihted)
        sprite2d mousePointer;  // menu mouse art
        sprite2d pipeFitters;  // menu title

        // variables for highscore setting
        const int numberOfHighscores = 10;  // Number of high scores to store
        int[] highscores = new int[numberOfHighscores]; // Array of high scores
        string[] highscoreNames = new string[numberOfHighscores];    // Array of high score names
        const int maxLetters = 12; // Maximum amount of characters in high score name
        Int16 whichChar = 0;    // Which character in name are you entering
        int charCount = 65;     // UniCode value of current character
        char[] highName = new char[maxLetters]; // Array of characters for creating high score string name

        KeyboardState lastKeyboardState;        // Last keyboard state

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            //Set screen resolution
            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 600;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            displayWidth = graphics.GraphicsDevice.Viewport.Width;
            displayHeight = graphics.GraphicsDevice.Viewport.Height;

            aspectRatio = (float)displayWidth / (float)displayHeight; // Calculate the aspect ratio of game window for 3D graphics
            cameraView.Y = (int)(cameraDistance / (Math.Tan(MathHelper.ToRadians(90 - fov / 2))));
            cameraView.X = cameraView.Y * aspectRatio;
            scaleFactor = displayHeight / (cameraView.Y * 2);
          
            base.Initialize();
        }


        // function that gets called to reset the game, everything that gets called when it resets goes in here
        void reset() 
        {
            int counter = 0;    // setting variable counter too an interger and starting it at 0
          
            // the following array is setting the puzzle pieces into there position on the screen and giving them specified level 
            for (int x = 0; x < 8; x++) 
            {
                for (int y = 0; y < 6; y++)
                {
                    puzzle[counter, 0].position = new Vector3(-640 + ((x + 1) * 140), -475 + ((y + 1) * 125), 0); // spacing the puzzles out 
                    counter++; // increasing the counter variable by increments of 1
                }
            }



            int count; // sets to count 
            
            // The following Array is to set the variables for the puzzle array
            for (count = 0; count < numberOfPuzzlePieces; count++) 
            {
                puzzle[count, 0].size = 2.5f; // gives the size of the puzzle pieces
                puzzle[count, 0].transforms = new Matrix[puzzle[count, 0].graphic.Bones.Count]; // Initializes the Transformations
                puzzle[count, 0].graphic.CopyAbsoluteBoneTransformsTo(puzzle[count, 0].transforms); // Copys Bone Structure and transforms from 3D graphics
                puzzle[count, 0].radius = (puzzle[count, 0].graphic.Meshes[0].BoundingSphere.Radius * puzzle[count, 0].size) * 0.5f; // sets bounding sphere radius
                puzzle[count, 0].width = 100; // width of Objects
            }


            // setting up the 3D spanners variables 
            spanner.size = 1f;
            spanner.transforms = new Matrix[spanner.graphic.Bones.Count];
            spanner.graphic.CopyAbsoluteBoneTransformsTo(spanner.transforms);
            spanner.rotation = new Vector3(MathHelper.ToRadians(0), 0, 0);
            spanner.radius = (spanner.graphic.Meshes[0].BoundingSphere.Radius * spanner.size) * 1.5f;
            spanner.bSphere = new BoundingSphere(new Vector3(0,0,0), spanner.radius);

            // the following sets the puzzle peice bounding boxs 
            for (int i = 0; i < numberOfPuzzlePieces; i++) 
            {
                puzzle[i, 0].bBox = new BoundingBox(new Vector3(puzzle[i, 0].position.X - puzzle[i, 0].radius, puzzle[i, 0].position.Y - puzzle[i, 0].radius, 0), new Vector3(puzzle[i, 0].position.X + puzzle[i, 0].radius, puzzle[i, 0].position.Y + puzzle[i, 0].radius, 0));
            }

            // the following sets the obstacle bsphere and says if it intersects with the puzzle bbox to position it inside it/ center it
            for (count = 0; count < numberOfObs; count++)
            {
                obs[count].bSphere = new BoundingSphere(obs[count].position, obs[count].radius); // sets obstacle bounding sphere

                for (int i = 0; i < numberOfPuzzlePieces; i++)
                {
                    if (obs[count].bSphere.Intersects(puzzle[i, 0].bBox)) // if obstacle bounding sphere touches puzzle bounding sphere center object to puzzle position
                    {
                        obs[count].position = puzzle[i, 0].position;
                    }
                }
            }

            level = 0; // sets the initial level to 0
            resetlevel(level); // Calls up reset level
            score = 0; // sets score to 0
            gameOverBool = false; // sets gameover bool to false
            lastKeyboardState = Keyboard.GetState(); // makes variable lastkeyboardstate into the Keyboard.GetState();
        }

        // function that resets specific levels, and there variables, pass in a level number to set the specified level 
        void resetlevel(int level) 
        {
            if (level == 0) // the following is setting the values and variables inside of level 1
            {
                numberOfPipes = 17; // number of pipes in this level
                
                // the values correspond to what should be in each box e.g anything greater than 6 specifys that
                // an obstacle such as an gold, silver or an entry/exit point is on that piece. anything below 6
                // corresponds to a pipe type and the orientation of it. These values are matched up against the 
                // puzzle[i, 1] settings to decide if the level is complete or not.

                puzzle[0, 0].value = 0; // setting the Value for each puzzle piece
                puzzle[1, 0].value = 0;
                puzzle[2, 0].value = 5;
                puzzle[3, 0].value = 1;
                puzzle[4, 0].value = 1;
                puzzle[5, 0].value = 12;

                puzzle[6, 0].value = 7;
                puzzle[7, 0].value = 7;
                puzzle[8, 0].value = 2;
                puzzle[9, 0].value = 7;
                puzzle[10, 0].value = 7;
                puzzle[11, 0].value = 7;

                puzzle[12, 0].value = 0;
                puzzle[13, 0].value = 8;
                puzzle[14, 0].value = 2;
                puzzle[15, 0].value = 8;
                puzzle[16, 0].value = 0;
                puzzle[17, 0].value = 0;

                puzzle[18, 0].value = 0;
                puzzle[19, 0].value = 8;
                puzzle[20, 0].value = 2;
                puzzle[21, 0].value = 8;
                puzzle[22, 0].value = 8;
                puzzle[23, 0].value = 8;

                puzzle[24, 0].value = 0;
                puzzle[25, 0].value = 8;
                puzzle[26, 0].value = 3;
                puzzle[27, 0].value = 1;
                puzzle[28, 0].value = 1;
                puzzle[29, 0].value = 6;

                puzzle[30, 0].value = 0;
                puzzle[31, 0].value = 8;
                puzzle[32, 0].value = 7;
                puzzle[33, 0].value = 7;
                puzzle[34, 0].value = 7;
                puzzle[35, 0].value = 2;

                puzzle[36, 0].value = 5;
                puzzle[37, 0].value = 1;
                puzzle[38, 0].value = 1;
                puzzle[39, 0].value = 1;
                puzzle[40, 0].value = 1;
                puzzle[41, 0].value = 4;

                puzzle[42, 0].value = 11;
                puzzle[43, 0].value = 9;
                puzzle[44, 0].value = 9;
                puzzle[45, 0].value = 9;
                puzzle[46, 0].value = 9;
                puzzle[47, 0].value = 9;


            }

            if (level == 1) // the following is setting the values and variables inside of level 2
            {
                numberOfPipes = 13; // number of pipes in this level

                puzzle[0, 0].value = 13; // setting the Value for each puzzle piece
                puzzle[1, 0].value = 1;
                puzzle[2, 0].value = 6;
                puzzle[3, 0].value = 0;
                puzzle[4, 0].value = 8;
                puzzle[5, 0].value = 0;

                puzzle[6, 0].value = 8;
                puzzle[7, 0].value = 8;
                puzzle[8, 0].value = 2;
                puzzle[9, 0].value = 0;
                puzzle[10, 0].value = 8;
                puzzle[11, 0].value = 0;

                puzzle[12, 0].value = 0;
                puzzle[13, 0].value = 8;
                puzzle[14, 0].value = 2;
                puzzle[15, 0].value = 0;
                puzzle[16, 0].value = 8;
                puzzle[17, 0].value = 0;

                puzzle[18, 0].value = 8;
                puzzle[19, 0].value = 5;
                puzzle[20, 0].value = 4;
                puzzle[21, 0].value = 0;
                puzzle[22, 0].value = 8;
                puzzle[23, 0].value = 0;

                puzzle[24, 0].value = 0;
                puzzle[25, 0].value = 2;
                puzzle[26, 0].value = 9;
                puzzle[27, 0].value = 7;
                puzzle[28, 0].value = 8;
                puzzle[29, 0].value = 7;

                puzzle[30, 0].value = 0;
                puzzle[31, 0].value = 3;
                puzzle[32, 0].value = 1;
                puzzle[33, 0].value = 1;
                puzzle[34, 0].value = 1;
                puzzle[35, 0].value = 6;

                puzzle[36, 0].value = 0;
                puzzle[37, 0].value = 8;
                puzzle[38, 0].value = 0;
                puzzle[39, 0].value = 8;
                puzzle[40, 0].value = 8;
                puzzle[41, 0].value = 2;

                puzzle[42, 0].value = 0;
                puzzle[43, 0].value = 0;
                puzzle[44, 0].value = 0;
                puzzle[45, 0].value = 0;
                puzzle[46, 0].value = 0;
                puzzle[47, 0].value = 11;

            }

            if (level == 2) // Level 3
            {
                numberOfPipes = 19; // number of pipes in this level

                puzzle[0, 0].value = 0; // setting the Value for each puzzle piece
                puzzle[1, 0].value = 5;
                puzzle[2, 0].value = 1;
                puzzle[3, 0].value = 6;
                puzzle[4, 0].value = 0;
                puzzle[5, 0].value = 0;

                puzzle[6, 0].value = 0;
                puzzle[7, 0].value = 2;
                puzzle[8, 0].value = 8;
                puzzle[9, 0].value = 2;
                puzzle[10, 0].value = 0;
                puzzle[11, 0].value = 0;

                puzzle[12, 0].value = 8;
                puzzle[13, 0].value = 2;
                puzzle[14, 0].value = 8;
                puzzle[15, 0].value = 2;
                puzzle[16, 0].value = 0;
                puzzle[17, 0].value = 0;

                puzzle[18, 0].value = 0;
                puzzle[19, 0].value = 2;
                puzzle[20, 0].value = 8;
                puzzle[21, 0].value = 3;
                puzzle[22, 0].value = 1;
                puzzle[23, 0].value = 13;

                puzzle[24, 0].value = 0;
                puzzle[25, 0].value = 2;
                puzzle[26, 0].value = 7;
                puzzle[27, 0].value = 7;
                puzzle[28, 0].value = 7;
                puzzle[29, 0].value = 7;

                puzzle[30, 0].value = 8;
                puzzle[31, 0].value = 2;
                puzzle[32, 0].value = 8;
                puzzle[33, 0].value = 5;
                puzzle[34, 0].value = 1;
                puzzle[35, 0].value = 12;

                puzzle[36, 0].value = 0;
                puzzle[37, 0].value = 2;
                puzzle[38, 0].value = 8;
                puzzle[39, 0].value = 2;
                puzzle[40, 0].value = 8;
                puzzle[41, 0].value = 0;

                puzzle[42, 0].value = 0;
                puzzle[43, 0].value = 3;
                puzzle[44, 0].value = 1;
                puzzle[45, 0].value = 4;
                puzzle[46, 0].value = 0;
                puzzle[47, 0].value = 0;

            }

            if (level == 3) // Level 4
            {
                numberOfPipes = 23; // number of pipes on this level

                puzzle[0, 0].value = 13; // setting the Value for each puzzle piece
                puzzle[1, 0].value = 1;
                puzzle[2, 0].value = 1;
                puzzle[3, 0].value = 1;
                puzzle[4, 0].value = 1;
                puzzle[5, 0].value = 6;

                puzzle[6, 0].value = 8;
                puzzle[7, 0].value = 8;
                puzzle[8, 0].value = 8;
                puzzle[9, 0].value = 8;
                puzzle[10, 0].value = 8;
                puzzle[11, 0].value = 2;

                puzzle[12, 0].value = 12;
                puzzle[13, 0].value = 6;
                puzzle[14, 0].value = 7;
                puzzle[15, 0].value = 5;
                puzzle[16, 0].value = 1;
                puzzle[17, 0].value = 4;

                puzzle[18, 0].value = 5;
                puzzle[19, 0].value = 4;
                puzzle[20, 0].value = 7;
                puzzle[21, 0].value = 2;
                puzzle[22, 0].value = 9;
                puzzle[23, 0].value = 9;

                puzzle[24, 0].value = 2;
                puzzle[25, 0].value = 7;
                puzzle[26, 0].value = 7;
                puzzle[27, 0].value = 2;
                puzzle[28, 0].value = 0;
                puzzle[29, 0].value = 9;

                puzzle[30, 0].value = 2;
                puzzle[31, 0].value = 7;
                puzzle[32, 0].value = 5;
                puzzle[33, 0].value = 4;
                puzzle[34, 0].value = 0;
                puzzle[35, 0].value = 9;

                puzzle[36, 0].value = 2;
                puzzle[37, 0].value = 7;
                puzzle[38, 0].value = 2;
                puzzle[39, 0].value = 8;
                puzzle[40, 0].value = 0;
                puzzle[41, 0].value = 9;

                puzzle[42, 0].value = 3;
                puzzle[43, 0].value = 1;
                puzzle[44, 0].value = 4;
                puzzle[45, 0].value = 0;
                puzzle[46, 0].value = 9;
                puzzle[47, 0].value = 9;

            }


            if (level == 4) // Level 5
            {
                numberOfPipes = 9; // number of pipes on this level

                puzzle[0, 0].value = 0; // setting the Value for each puzzle piece
                puzzle[1, 0].value = 10;
                puzzle[2, 0].value = 0;
                puzzle[3, 0].value = 0;
                puzzle[4, 0].value = 0;
                puzzle[5, 0].value = 0;

                puzzle[6, 0].value = 0;
                puzzle[7, 0].value = 3;
                puzzle[8, 0].value = 1;
                puzzle[9, 0].value = 1;
                puzzle[10, 0].value = 6;
                puzzle[11, 0].value = 0;

                puzzle[12, 0].value = 8;
                puzzle[13, 0].value = 8;
                puzzle[14, 0].value = 0;
                puzzle[15, 0].value = 8;
                puzzle[16, 0].value = 2;
                puzzle[17, 0].value = 0;

                puzzle[18, 0].value = 0;
                puzzle[19, 0].value = 0;
                puzzle[20, 0].value = 0;
                puzzle[21, 0].value = 8;
                puzzle[22, 0].value = 2;
                puzzle[23, 0].value = 0;

                puzzle[24, 0].value = 0;
                puzzle[25, 0].value = 8;
                puzzle[26, 0].value = 8;
                puzzle[27, 0].value = 8;
                puzzle[28, 0].value = 2;
                puzzle[29, 0].value = 0;

                puzzle[30, 0].value = 0;
                puzzle[31, 0].value = 0;
                puzzle[32, 0].value = 0;
                puzzle[33, 0].value = 8;
                puzzle[34, 0].value = 2;
                puzzle[35, 0].value = 0;

                puzzle[36, 0].value = 9;
                puzzle[37, 0].value = 9;
                puzzle[38, 0].value = 0;
                puzzle[39, 0].value = 9;
                puzzle[40, 0].value = 3;
                puzzle[41, 0].value = 13;

                puzzle[42, 0].value = 0;
                puzzle[43, 0].value = 0;
                puzzle[44, 0].value = 11;
                puzzle[45, 0].value = 9;
                puzzle[46, 0].value = 0;
                puzzle[47, 0].value = 0;

            }

            if (level == 5) // Level 6
            {
                numberOfPipes = 20; // number of pipes on this level

                puzzle[0, 0].value = 0; // setting the Value for each puzzle piece
                puzzle[1, 0].value = 0;
                puzzle[2, 0].value = 0;
                puzzle[3, 0].value = 0;
                puzzle[4, 0].value = 5;
                puzzle[5, 0].value = 12;

                puzzle[6, 0].value = 0;
                puzzle[7, 0].value = 8;
                puzzle[8, 0].value = 8;
                puzzle[9, 0].value = 5;
                puzzle[10, 0].value = 4;
                puzzle[11, 0].value = 8;

                puzzle[12, 0].value = 0;
                puzzle[13, 0].value = 8;
                puzzle[14, 0].value = 5;
                puzzle[15, 0].value = 4;
                puzzle[16, 0].value = 8;
                puzzle[17, 0].value = 0;

                puzzle[18, 0].value = 0;
                puzzle[19, 0].value = 5;
                puzzle[20, 0].value = 4;
                puzzle[21, 0].value = 8;
                puzzle[22, 0].value = 5;
                puzzle[23, 0].value = 13;

                puzzle[24, 0].value = 5;
                puzzle[25, 0].value = 4;
                puzzle[26, 0].value = 8;
                puzzle[27, 0].value = 5;
                puzzle[28, 0].value = 4;
                puzzle[29, 0].value = 0;

                puzzle[30, 0].value = 2;
                puzzle[31, 0].value = 8;
                puzzle[32, 0].value = 5;
                puzzle[33, 0].value = 4;
                puzzle[34, 0].value = 8;
                puzzle[35, 0].value = 0;

                puzzle[36, 0].value = 2;
                puzzle[37, 0].value = 8;
                puzzle[38, 0].value = 2;
                puzzle[39, 0].value = 0;
                puzzle[40, 0].value = 0;
                puzzle[41, 0].value = 0;

                puzzle[42, 0].value = 3;
                puzzle[43, 0].value = 1;
                puzzle[44, 0].value = 4;
                puzzle[45, 0].value = 0;
                puzzle[46, 0].value = 0;
                puzzle[47, 0].value = 0;

            }



            numberOfObs = 0; // sets the number of obstacles to 0

            // The following Array is saving the puzzle then reseting the values that are less than < 6 to 0 
            // one is basically the correct finished version, the other is the one being updated constantly as
            // the player puts the pieces down
            for (int i = 0; i < numberOfPuzzlePieces; i++) 
            {
                puzzle[i, 1] = puzzle[i, 0]; // set puzzle[i, 1] to puzzle[i, 0] 
                if (puzzle[i, 0].value <= 6) // if the values on the puzzle pieces are less than 6 change them to 0
                    puzzle[i, 0].value = 0;


                if (puzzle[i, 0].value > 6) // if value of puzzle is > than 6 do the following
                {
                    if (puzzle[i, 0].value == 7) // if the value of the puzzle is 7 then load up model "gems" to it
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("gems");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                    }

                    // if the puzzle value is 8 then load model gold into it and position it inside
                    if (puzzle[i, 0].value == 8)
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("gold");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                    }

                    // if the puzzle value is 9 then load model bone into it and position it inside 
                    if (puzzle[i, 0].value == 9)
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("bone");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                    }

                    // if the puzzle value is 10 then load model start into it and position it inside, and rotate it
                    if (puzzle[i, 0].value == 10)
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("start");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                        obs[numberOfObs].rotation.Z = MathHelper.ToRadians(0);
                    }

                    // if the puzzle value is 11 then load model finish into it and position it inside, and rotate it
                    if (puzzle[i, 0].value == 11)
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("finish");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                        obs[numberOfObs].rotation.Z = MathHelper.ToRadians(0);
                    }

                    // if the puzzle value is 12 then load model start into it and position it inside, and rotate it
                    if (puzzle[i, 0].value == 12)
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("start");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                        obs[numberOfObs].rotation.Z = MathHelper.ToRadians(90);
                    }

                    // if the puzzle value is 13 then load model finish into it and position it inside, and rotate it
                    if (puzzle[i, 0].value == 13)
                    {
                        obs[numberOfObs].graphic = Content.Load<Model>("finish");
                        obs[numberOfObs].position = puzzle[i, 0].position;
                        obs[numberOfObs].rotation.Z = MathHelper.ToRadians(90);
                    }

                    levelCompleteBool = false; // set bool level over too false
                    numberOfObs++; // number of obstacles ++
                }

                for (int count = 0; count < numberOfObs; count++)
                {
                    obs[count].size = 2.5f; //size of obstacle
                    obs[count].transforms = new Matrix[obs[count].graphic.Bones.Count]; // create new matrix
                    obs[count].graphic.CopyAbsoluteBoneTransformsTo(obs[count].transforms); // load bone transformations
                    obs[count].radius = (obs[count].graphic.Meshes[0].BoundingSphere.Radius * obs[count].size) * 4f; // set obstacle radius
                    obs[count].width = 100; // obstacle width
                }

                // the following array sets the variables for both types of pipes
                for (int count = 0; count < numberOfPipes; count++)
                {
                    pipe[count, 0].size = 2.5f;
                    pipe[count, 0].transforms = new Matrix[pipe[count, 0].graphic.Bones.Count];       // make an array of transforms, one for each 'bone' in the 3d model
                    pipe[count, 0].graphic.CopyAbsoluteBoneTransformsTo(pipe[count, 0].transforms);   // copy the transforms from the 3d model into this array ready for use
                    pipe[count, 0].radius = (pipe[count, 0].graphic.Meshes[0].BoundingSphere.Radius * pipe[count, 0].size) * 1.2f;       // Gets the radius of the meshes (3D Model)
                    pipe[count, 1].transforms = new Matrix[pipe[count, 1].graphic.Bones.Count];       // make an array of transforms, one for each 'bone' in the 3d model
                    pipe[count, 1].graphic.CopyAbsoluteBoneTransformsTo(pipe[count, 1].transforms);   // copy the transforms from the 3d model into this array ready for use
                    pipe[count, 1].radius = (pipe[count, 1].graphic.Meshes[0].BoundingSphere.Radius * pipe[count, 1].size) * 1.2f;       // Gets the radius of the meshes (3D Model)
                    pipe[count, 0].rotation = new Vector3(MathHelper.ToRadians(0), 0, MathHelper.ToRadians(0));
                    pipe[count, 0].pipeType = 0;
                    pipe[count, 0].position = new Vector3(0, cameraView.Y - 50, 0);
                    pipe[count, 0].bSphere = new BoundingSphere(pipe[count, 0].position, pipe[count, 0].radius); // Set bounding sphere
                }
            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // TODO: use this.Content to load your game content here
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            firstLevel = Content.Load<SoundEffect>("level1+"); // Load music in
            music1 = firstLevel.CreateInstance(); // create instance for music
            music1.IsLooped = true; // loop music
            music1.Volume = 0.5f ; // set music volume

            fourthLevel = Content.Load<SoundEffect>("level3+"); // load music in
            music2 = fourthLevel.CreateInstance(); // create instance for music
            music2.IsLooped = true; // loop music
            music2.Volume = 0.5f; // set music volume


            fontWhite = Content.Load<SpriteFont>("miramonte9"); // Load font
            digitalFont = Content.Load<SpriteFont>("quartz4"); // Load font
            gameOverScreen = new graphic2d(Content, "gameover", displayWidth, displayHeight); // Load 2D graphic
            menuBackground = new graphic2d(Content, "mainmenu2", displayWidth, displayHeight); // Load 2D graphic
           
            // the following is loading in the 2 different types of menu pictures 1 for selection and one when not selected
            menuChoices[0, 0] = new sprite2d(Content, "playgame", displayWidth / 2, 90, 1, Color.Wheat, true); 
            menuChoices[0, 0].updatebox();
            menuChoices[0, 1] = new sprite2d(Content, "playgame2", displayWidth / 2, 90, 1, Color.Wheat, true);

            menuChoices[1, 0] = new sprite2d(Content, "tutorial", displayWidth / 2, 230, 1, Color.Wheat, true);
            menuChoices[1, 0].updatebox();
            menuChoices[1, 1] = new sprite2d(Content, "tutorial2", displayWidth / 2, 230, 1, Color.Wheat, true);

            menuChoices[2, 0] = new sprite2d(Content, "scoreboard", displayWidth / 2, 360, 1, Color.Wheat, true);
            menuChoices[2, 0].updatebox();
            menuChoices[2, 1] = new sprite2d(Content, "scoreboard2", displayWidth / 2, 360, 1, Color.Wheat, true);
            
            menuChoices[3, 0] = new sprite2d(Content, "quit", displayWidth / 2, 490, 1, Color.Wheat, true);
            menuChoices[3, 0].updatebox();
            menuChoices[3, 1] = new sprite2d(Content, "quit2", displayWidth / 2, 490, 1, Color.Wheat, true);


            int count;
            for (count = 0; count < 48; count++)
            {
                pipe[count, 0].graphic = Content.Load<Model>("pipe1");                // Load the 3D model from the ContentManager
                pipe[count, 1].graphic = Content.Load<Model>("bendpipe");                // Load the 3D model from the ContentManager
            }

            for (count = 0; count < numberOfPuzzlePieces; count++)
            {
                puzzle[count, 0].graphic = Content.Load<Model>("grid"); // Load the 3D model from the ContentManager
            }




            // setting spanner graphic
            spanner.graphic = Content.Load<Model>("spanner");

            pipeDrop = Content.Load<SoundEffect>("Pipedrop"); // load sound
            gameOver = Content.Load<SoundEffect>("gameover1"); // load sound

            // loading in and setting up background images
            youWin = new graphic2d();
            youWin.image = Content.Load<Texture2D>("youwin"); // Load gameover image
            float ratio = ((float)displayWidth / youWin.image.Width); // Work out the ratio for the image depending on screen size
            youWin.rect.Width = displayWidth;
            youWin.rect.Height = (int)(youWin.image.Height * ratio); // Work out new height based on the ratio
            youWin.rect.X = 0;
            youWin.rect.Y = (displayHeight - youWin.rect.Height) / 2;
            
            gameOverImage = new graphic2d();
            gameOverImage.image = Content.Load<Texture2D>("gameover");
            ratio = ((float)displayWidth / gameOverImage.image.Width); // Work out the ratio for the image depending on screen size
            gameOverImage.rect.Width = displayWidth;
            gameOverImage.rect.Height = (int)(gameOverImage.image.Height * ratio); // Work out new height based on the ratio
            gameOverImage.rect.X = 0;
            gameOverImage.rect.Y = (displayHeight - gameOverImage.rect.Height) / 2; // Put image in the middle of the screen on the Y axis
           
            tutorial = new graphic2d();
            tutorial.image = Content.Load<Texture2D>("maingameHELP");
            ratio = ((float)displayWidth / tutorial.image.Width); // Work out the ratio for the image depending on screen size
            tutorial.rect.Width = displayWidth;
            tutorial.rect.Height = (int)(tutorial.image.Height * ratio); // Work out new height based on the ratio
            tutorial.rect.X = 0;
            tutorial.rect.Y = (displayHeight - tutorial.rect.Height) / 2; // Put image in the middle of the screen on the Y axis
      
            // loading in and setting up sprites
            mousePointer = new sprite2d(Content, "con_front", 0, 0, 0.5f, Color.Wheat, true); // set mouse pointer to a new 
            pipeFitters = new sprite2d(Content, "Pipefitters", 770, 300, 1f, Color.Black, true); // load up Pipefitters logo and position it 
            pipeFitters.rotation = MathHelper.ToRadians(90); // rotate pipe fitters logo and change it to radians


            // Load high scores in
            if (File.Exists(@"highscores.txt")) // This checks to see if the file exists
            {
                String line;		// Create a string variable to read each line into
                StreamReader sr = new StreamReader(@"highscores.txt");	// Open the file

                // Read in high scores
                for (int i = 0; i < numberOfHighscores; i++)
                {
                    // read in names
                    do
                    {
                        line = sr.ReadLine();
                    } 
                    while (String.IsNullOrEmpty(line) && !sr.EndOfStream);
                    
                    line = line.Trim();
                    highscoreNames[i] = line.PadRight(maxLetters, ' ');
                    
                    // read in scores
                    do
                    {
                        line = sr.ReadLine();
                    }
                    while (String.IsNullOrEmpty(line) && !sr.EndOfStream);
                    
                    line = line.Trim();
                    highscores[i] = (int)Convert.ToDecimal(line);
                }
                sr.Close();			// Close the file
            }
            else // if no file create blank values
            {
                for (int i = 0; i < numberOfHighscores; i++)
                {
                    highscores[i] = 0;
                    highscoreNames[i] = " ";
                }
            }

            reset(); // Set up initial values
        }

        // the following is called to check if the puzzle is complete, called repeatedly during update while in game
        Boolean puzzlecomplete() 
        {
            bool complete = true; // set boolean complete too true

            // the following is if the saved puzzle value is not the same as the current puzzle value then level is incomplete
            for (int i = 0; i < numberOfPuzzlePieces; i++) 
                if (puzzle[i, 1].value != puzzle[i, 0].value)
                    complete = false;

            return complete; // put complete back to its original state
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            StreamWriter sw = new StreamWriter(@"highscores.txt"); 
            // streaming out the high scores into a file
            for (int i = 0; i < numberOfHighscores; i++)
            {
                sw.WriteLine(highscoreNames[i]);
                sw.WriteLine(highscores[i].ToString());
            }
            sw.Close();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here

            // Reads Input from keyboard and mouse
            KeyboardState keys = Keyboard.GetState();                   // Read keyboard
            MouseState mouse = Mouse.GetState();
            
            // checks if no keys/the same key is not currently being pressed 
            keyReleased = (lastKeyboardState != keys);

            // this case switch acts as a Game State controller, gameMode is a passed in
            // int that switches the case states
            switch (gameMode)
            {
                case 0: // main menu state
      
                    // Allow mouse to select menu options, updates it's position and boundingbox
                    mousePointer.position.X = mouse.X;
                    mousePointer.position.Y = mouse.Y;
                    mousePointer.updatebox();
                   
                    // checks if the mouse is over a menu choice via intersectiong bounding boxes
                    // then sets menuSelection to the relevant choice
                    for (int i = 0; i < numOfChoices; i++)
                        if (mousePointer.bbox.Intersects(menuChoices[i, 0].bbox))
                            menuSelection = i;

                    // Allows keyboard to control the menu option choice, via up and down keys
                    if (keys.IsKeyDown(Keys.Up) && keyReleased)
                    {
                        menuSelection--;
                        keyReleased = false;
                    }
                    if (keys.IsKeyDown(Keys.Down) && keyReleased)
                    {
                        menuSelection++;
                        keyReleased = false;
                    }

                    // Set limits for selections, don't want it to be over the max or under the min
                    if (menuSelection < 0) menuSelection = 0;
                    if (menuSelection >= numOfChoices) menuSelection = numOfChoices - 1;

                    // if enter or left mouse button is pressed down it chooses the option and switches
                    // the state via the current menuSelection, needs to be increased by +1 since we 
                    // don't use gameMode 0 via the main menu as that IS the main menu state
                    if ((mouseReleased && mouse.LeftButton == ButtonState.Pressed) 
                        || (keyReleased && keys.IsKeyDown(Keys.Enter)))
                    {
                        gameMode = menuSelection + 1;
                    }

                    // if the gameMode is play game then reset the game, timer and start the music
                    if (gameMode == 1)
                    {
                        if (music1.State == SoundState.Stopped && level >= 0 && level < 3) // if the music stops and the level is between 0 and 3 play music 1
                        {
                            music1.Play();
                        }
                        gameStartTime = (float)gameTime.TotalGameTime.TotalSeconds; // Set start time of game
                        reset();
                    }

                    break;

                case 1: // game play state

                    // if enter is pressed, set back to the main menu and stop the music
                    if (keys.IsKeyDown(Keys.Escape))
                    {
                        gameMode = 0;
                        music1.Stop();
                        music2.Stop();
                    }

                    // if the game is not over and the level is incomplete run the following
                    if (!gameOverBool)
                    {
                        if (!levelCompleteBool)
                        {
                            gameTimeGone = (float)gameTime.TotalGameTime.TotalSeconds - gameStartTime; // Update game time gone
                            if (gameTimeGone >= lengthOfRound)
                                gameOverBool = true;

                            // Set position of spanner based on the mouse
                            spanner.position.X = (mouse.X - (displayWidth / 2)) / scaleFactor;
                            spanner.position.Y = (-mouse.Y + (displayHeight / 2)) / scaleFactor;
                            spanner.bSphere.Center = spanner.position;
                           

                            // the following is for sticking and unsticking the pipe to the mouse, -1 being nothing stuck
                            if (stick == -1)
                            {
                                // press left button to pick something up
                                if (mouse.LeftButton == ButtonState.Pressed && mouseReleased)
                                {
                                    for (int count = 0; count < numberOfPipes; count++)
                                    {
                                        // updating bounding sphere can cause minor bounding issues otherwise
                                        pipe[count, 0].bSphere.Center = pipe[count, 0].position;

                                        if (spanner.bSphere.Intersects(pipe[count, 0].bSphere))
                                        {
                                            // Select current pipe to stick to spanner
                                            stick = count;
                                            
                                            // if pipe removed from a square and reset the puzzle pieces value
                                            for (int i = 0; i < numberOfPuzzlePieces; i++)
                                                if (pipe[stick, 0].bSphere.Intersects(puzzle[i, 0].bBox))
                                                    puzzle[i, 0].value = 0;
                                        }
                                    }
                                }
                            }
                            else // if the mouse has a pipe already attached do the following
                            {
                                if (mouse.LeftButton == ButtonState.Pressed && mouseReleased)
                                {
                                    // Set and update bounding sphere around selected pipe as it is moving
                                    pipe[stick, 0].bSphere.Center = pipe[stick, 0].position;

                                    // the following is for droping pipes into the centre of puzzle pieces and changing the puzzles values according to type of pipe
                                    for (int i = 0; i < numberOfPuzzlePieces; i++)
                                    {
                                        if (pipe[stick, 0].bSphere.Intersects(puzzle[i, 0].bBox)) 
                                        {
                                            pipe[stick, 0].position = puzzle[i, 0].position;

                                            if (puzzle[i, 0].value == 0) // if the puzzle pieces value is 0 and one of the following pipes is put onto it change the value accordingly
                                            {
                                           
                                                if (pipe[stick, 0].pipeType == 0 && (pipe[stick, 0].rotation.Z == MathHelper.ToRadians(0) || pipe[stick, 0].rotation.Z == MathHelper.ToRadians(180)))
                                                    puzzle[i, 0].value = 2;
                                                if (pipe[stick, 0].pipeType == 0 && (pipe[stick, 0].rotation.Z == MathHelper.ToRadians(90) || pipe[stick, 0].rotation.Z == MathHelper.ToRadians(270)))
                                                    puzzle[i, 0].value = 1;
                                                if (pipe[stick, 0].pipeType == 1 && pipe[stick, 0].rotation.Z == MathHelper.ToRadians(0))
                                                    puzzle[i, 0].value = 3;
                                                if (pipe[stick, 0].pipeType == 1 && pipe[stick, 0].rotation.Z == MathHelper.ToRadians(90))
                                                    puzzle[i, 0].value = 4;
                                                if (pipe[stick, 0].pipeType == 1 && pipe[stick, 0].rotation.Z == MathHelper.ToRadians(180))
                                                    puzzle[i, 0].value = 6;
                                                if (pipe[stick, 0].pipeType == 1 && pipe[stick, 0].rotation.Z == MathHelper.ToRadians(270))
                                                    puzzle[i, 0].value = 5;

                                                // Unstick pipe & drop it  
                                                stick = -1;
                                                pipeDrop.Play(); // Play sound
                                                break; // quick fix breaks out of the for/if stops the index from going out of bounds
                                            }
                                        }
                                    }
                                }

                                // A pipe is stuck to the spanner
                                if (stick > -1)
                                {
                                    // If pipe is stuck to spanner move it with the spanner
                                    pipe[stick, 0].position = spanner.position;

                                    // Cycle through different pipe types
                                    if (Math.Abs(scrollValue - mouse.ScrollWheelValue) >= 60)
                                    {
                                        if (pipe[stick, 0].pipeType == 0)
                                            pipe[stick, 0].pipeType = 1;
                                        else
                                            pipe[stick, 0].pipeType = 0;

                                        scrollValue = mouse.ScrollWheelValue;
                                    }

                                    // Rotate pipe
                                    if (mouse.RightButton == ButtonState.Pressed && mouseReleased)
                                    {
                                        pipe[stick, 0].rotation += new Vector3(MathHelper.ToRadians(0), 0, MathHelper.ToRadians(90));
                                        if (pipe[stick, 0].rotation.Z >= MathHelper.ToRadians(360))
                                            pipe[stick, 0].rotation.Z = 0;
                                    }
                                }
                            }

                            levelCompleteBool = puzzlecomplete(); // set level over to puzzle complete
                            if (levelCompleteBool) gameOver.Play(); // if levelover initates play gameover1 music
                        }
                             

                        else // if level is complete
                        {
                            // stop music
                            music1.Stop();
                            music2.Stop();
                            
                            // start music again when space or left mouse button pressed (moving onto next level)
                            if ((keys.IsKeyDown(Keys.Space) || mouse.LeftButton == ButtonState.Pressed) && mouseReleased)
                            {
                                if (music1.State == SoundState.Stopped && level >= 0 && level < 2)
                                {
                                    music1.Play();
                                }
                                if (music2.State == SoundState.Stopped && level >= 2)
                                {
                                    music2.Play();
                                }

                                gameStartTime = (float)gameTime.TotalGameTime.TotalSeconds; // Set start time of game
                                level++; // increase the level
                                score += (int)((lengthOfRound - gameTimeGone) * 5 + level * 100);  // the way the score is calculated

                                // If they have reached last level end game
                                if (level == numberOfLevels)
                                    gameOverBool = true;
                                else
                                    resetlevel(level);
                            }
                        }
                    }
                    else // the following is called if the game is over
                    {
                        // stop music play game over noise
                        music1.Stop();
                        music2.Stop();
                        gameOver.Play();

                        if (score > highscores[9])
                        {
                            // setting up initial highscore values            
                            whichChar = 0;
                            highName[0] = 'A';

                            for (int i = 1; i < maxLetters; i++)
                                highName[i] = ' ';
                        }

                        gameMode = 5; // set to highscore entry page
                    }

                    break;
                
                case 2: // tutorial
                    if ((mouse.LeftButton == ButtonState.Pressed && mouseReleased) || keys.IsKeyDown(Keys.Escape)) // if any of these buttons are pressed go back to the main menu
                        gameMode = 0;

                    break;

                case 3: // scoreboard
                    if ((mouse.LeftButton == ButtonState.Pressed && mouseReleased) || keys.IsKeyDown(Keys.Escape)) // if any of these buttons are pressed go back to the main menu
                        gameMode = 0;

                    break;

                case numOfChoices: // quit game
                    this.Exit();
                    break;

                case 5: // high score entry page
                    if (score > highscores[9]) // if the score is higher than the lowest highscore, allow them to add
                    {                          // there name and score to the list
                     
                        Boolean finished = false;
                        int whichChar2 = whichChar;

                        charCount = Convert.ToUInt16(highName[whichChar]); // Convert character to UniCode value
                       
                        if (keyReleased)
                        {
                            // Allow user to move left and right along characters to select which letter to change
                            if (keys.IsKeyDown(Keys.Left)) whichChar--;
                            if (keys.IsKeyDown(Keys.Right)) whichChar++;

                            // Choose a new character by pressing up or down arrow keys
                            if (keys.IsKeyDown(Keys.Up))
                                charCount++;
                            if (keys.IsKeyDown(Keys.Down))
                                charCount--;
                        }

                        // Allow user to enter name using mouse
                        if (mouseReleased)
                        {
                            if (mouse.LeftButton == ButtonState.Released) whichChar--;
                            if (mouse.RightButton == ButtonState.Released) whichChar++;

                            if ((scrollValue - mouse.ScrollWheelValue) <= -60)
                            {
                                charCount++;
                                scrollValue = mouse.ScrollWheelValue;
                            }
                            else if ((scrollValue - mouse.ScrollWheelValue) >= 60)
                            {
                                charCount--;
                                scrollValue = mouse.ScrollWheelValue;
                            }
                        }

                        // Check that character is within range
                        if (whichChar >= maxLetters)
                        {
                            whichChar = maxLetters - 1;
                            finished = true;
                        }
                    
                        // if you go below the minimum amount of chars set back to 0
                        if (whichChar < 0) whichChar = 0;

                        // Set range of selectable characters
                        if (charCount > 90) charCount = 32;
                        if (charCount < 32) charCount = 90;
                        if (charCount == 64) charCount = 32;
                        if (charCount == 33) charCount = 65;
                        
                        // change charCount if the character place in the string
                        // you've moved to is not the same as previous place 
                        if (whichChar != whichChar2)
                            charCount = (char)highName[whichChar];

                        // Store character into array of characters and then into string
                        highName[whichChar] = (char)charCount;
                        highscoreNames[9] = new string(highName);

                        // if enter is pressed or max number of character positions reached
                        // enter name into highscore table and sort them then set state 
                        // to the highscore state
                        if ((keyReleased && keys.IsKeyDown(Keys.Enter)) || finished)
                        {
                            highscores[9] = score;
                            Array.Sort(highscores, highscoreNames);
                            Array.Reverse(highscores);
                            Array.Reverse(highscoreNames);
                            gameMode = 3;
                        }
                    }
                    else
                    {
                        if ((mouse.LeftButton == ButtonState.Pressed && mouseReleased) || keys.IsKeyDown(Keys.Escape))
                            gameMode = 0;
                    }


                    break;

                default:
                    // Do nothing if none of the above are selected
                    break;
            }
         
            // get the keyboard state
            lastKeyboardState = Keyboard.GetState();
            
            // change mouseReleased to true or false based on mouse state 
            mouseReleased = (mouse.RightButton == ButtonState.Released && mouse.LeftButton == ButtonState.Released);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            switch (gameMode)
            {
                case 0: // draw for main menu
                    
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                    
                    //draw background
                   spriteBatch.Draw(menuBackground.image, menuBackground.rect, Color.White);
                   
                    
                    // Draw main menu options
                    for (int i = 0; i < numOfChoices; i++)
                    {
                        if (i == menuSelection)
                            menuChoices[i, 1].drawme(ref spriteBatch);
                        else
                            menuChoices[i, 0].drawme(ref spriteBatch);
                    }

                    // draw mouse pointer and title
                    pipeFitters.drawme(ref spriteBatch);
                    mousePointer.drawme(ref spriteBatch);
                    
                    spriteBatch.End();

                    break;

                case 1: // in game draw 
                    // TODO: Add your drawing code here
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                    
                    // Draw Scores and number of lives on screen
                    spriteBatch.DrawString(digitalFont, "Time Left " + (lengthOfRound - gameTimeGone).ToString("00"), new Vector2(510, 0), Color.White);
                    spriteBatch.DrawString(digitalFont, "Current Level " + (level + 1).ToString(), new Vector2(250, 565), Color.White);
                    spriteBatch.DrawString(digitalFont, "Score " + score.ToString(), new Vector2(35, 0), Color.White);
                    spriteBatch.End();

                    // resetgraphics for 3D models
                    sfunctions.resetgraphics(GraphicsDevice);

                    int count;
                    // draw puzzle pieces
                    for (count = 0; count < numberOfPuzzlePieces; count++)
                        sfunctions.drawmesh(puzzle[count, 0].position, puzzle[count, 0].rotation, puzzle[count, 0].size, puzzle[count, 0].graphic,
                            puzzle[count, 0].transforms, Vector3.Up, true, cameraPosition, cameraLookAt, fov, aspectRatio);

                    // draw obstacles
                    for (count = 0; count < numberOfObs; count++)
                        sfunctions.drawmesh(obs[count].position, obs[count].rotation, obs[count].size, obs[count].graphic, obs[count].transforms,
                            Vector3.Up, true, cameraPosition, cameraLookAt, fov, aspectRatio);


                  

                    // draw pipes
                    for (count = 0; count < numberOfPipes; count++)
                    {
                        sfunctions.drawmesh(pipe[count, 0].position, pipe[count, 0].rotation, pipe[count, 0].size, pipe[count, pipe[count, 0].pipeType].graphic, 
                            pipe[count, pipe[count, 0].pipeType].transforms, Vector3.Up, true, cameraPosition, cameraLookAt, fov, aspectRatio);
                    }

                    // setting spanner position to be slightly nearer to the camera than the rest of the 
                    // objects so it won't go through/z fight with other models, doing with temp to not effect
                    // the actual position of the spanners bbox etc, so it can still colide with pipes etc but look
                    // closer
                    Vector3 temppos;
                    temppos = spanner.position;
                    temppos.Z += 10;

                    // draw spanner
                    sfunctions.drawmesh(temppos, spanner.rotation, spanner.size, spanner.graphic, spanner.transforms, Vector3.Up, true, 
                        cameraPosition, cameraLookAt, fov, aspectRatio);

                    // start 2d rendering
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                    if (levelCompleteBool) // if level over draw image
                        spriteBatch.Draw(youWin.image, youWin.rect, Color.White);
                   
                    if (gameOverBool) // if gameover draw image
                        spriteBatch.Draw(gameOverImage.image, gameOverImage.rect, Color.White); // If the game is over draw an image saying so

                    spriteBatch.End();                            
                    
                    break;

                case 2: // tutorial draw

                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                    
                    // draw tutorial screen
                    spriteBatch.Draw(tutorial.image, tutorial.rect, Color.White);

                    spriteBatch.End();
                   
                    break;

                case 3: // highscore draw
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);

                    // clear background to color
                    GraphicsDevice.Clear(Color.Gold);

                    // draw various strings to screen such as page title and highscores 
                    spriteBatch.DrawString(fontWhite, "High Score Table", new Vector2(50, 50), Color.Black , 0, new Vector2(0, 0), 2f, SpriteEffects.None, 0);
                    
                    for (int i = 0; i < numberOfHighscores; i++)
                    {
                        spriteBatch.DrawString(digitalFont, highscoreNames[i], new Vector2(100, 140 + (i * 40)), Color.Black , 0, new Vector2(0, 0), 1f, SpriteEffects.None, 0);
                        spriteBatch.DrawString(digitalFont, highscores[i].ToString(), new Vector2(450, 140 + (i * 40)), Color.Black , 0, new Vector2(0, 0), 1f, SpriteEffects.None, 0);
                    }
                    spriteBatch.End();

                    break;

                case 5: // highscore addition screen
                    spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                    
                    //draw background
                    gameOverScreen.drawme(ref spriteBatch);

                    // draw string if game completed
                    if (level == numberOfLevels)
                            spriteBatch.DrawString(digitalFont, "*WELL DONE YOU COMPLETED THE GAME*", new Vector2(displayWidth / 2 - 345, 100), Color.AliceBlue);

                    // if score is higher than the lowest draw the enter highscore string/name entry
                    if (score > highscores[9])
                    {
                        spriteBatch.DrawString(digitalFont, "New High Score Enter Name", new Vector2(displayWidth/2 - 205, 140), Color.White);
                        for (int i = 0; i < maxLetters; i++)
                            if (i == whichChar)
                                spriteBatch.DrawString(digitalFont, char.ToString(highName[i]), new Vector2(336 + (i * 21), 320), Color.Red, 0, new Vector2(0, 0), 0.9f, SpriteEffects.None, 0);
                            else
                                spriteBatch.DrawString(digitalFont, char.ToString(highName[i]), new Vector2(336 + (i * 21), 320), Color.White, 0, new Vector2(0, 0), 0.9f, SpriteEffects.None, 0);
                    }

                    // draw score to screen

                    spriteBatch.DrawString(digitalFont, "Score "+score.ToString(), new Vector2(360, 410), Color.White);
                    
                    spriteBatch.End();


                    break;

                default:
                    break;
            }


            base.Draw(gameTime);
        }
    }
}
