using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace standardfunctions
{
    public static class sfunctions
    {
        // Reset graphics device for 3D drawing
        public static void resetgraphics(GraphicsDevice graphics)
        {
            // These lines reset the graphics device for drawing 3D. 
            // This is only necessary if you are drawing 2D and 3D together.

            graphics.BlendState = BlendState.Opaque;
            graphics.DepthStencilState = DepthStencilState.Default;
        }

        // This method draws a 3D model
        public static void drawmesh(Vector3 position, Vector3 rotation, float scale, Model graphic, Matrix[] transforms, Vector3 orient, Boolean lightson, Vector3 camerapos,
                               Vector3 camlookat, float fov, float aspectrat)
        {
            // Quaternion rot = Quaternion.Normalize(Quaternion.CreateFromAxisAngle(Vector3.Up, rotation.Y) * Quaternion.CreateFromAxisAngle(Vector3.Right, rotation.X)  * Quaternion.CreateFromAxisAngle(Vector3.Backward, rotation.Z));
            foreach (ModelMesh mesh in graphic.Meshes)               // loop through the mesh in the 3d model, drawing each one in turn.
            {
                foreach (BasicEffect effect in mesh.Effects)                // This loop then goes through every effect in each mesh.
                {
                    if (lightson) effect.EnableDefaultLighting();           // Enables default lighting when lightson==TRUE, this can do funny things with textured on 3D models.
                    effect.PreferPerPixelLighting = true;                   // Makes it shiner and reflects light better
                    effect.World = transforms[mesh.ParentBone.Index];       // begin dealing with transforms to render the object into the game world
                    effect.World *= Matrix.CreateScale(scale);              // scale the mesh to the right size
                    effect.World *= Matrix.CreateRotationX(rotation.X);     // rotate the mesh
                    effect.World *= Matrix.CreateRotationY(rotation.Y);     // rotate the mesh
                    effect.World *= Matrix.CreateRotationZ(rotation.Z);     // rotate the mesh
                    //effect.World *= Matrix.CreateFromQuaternion(rot);     // Rotate the mesh using Quaternions (This may work better if you are rotating multiple axes)
                    //effect.World *= Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z); // rotate the mesh
                    effect.World *= Matrix.CreateTranslation(position);     // position the mesh in the game world

                    effect.View = Matrix.CreateLookAt(camerapos, camlookat, orient);    // Set the position of the camera and tell it what to look at

                    // Sets the FOV (Field of View) of the camera. The first paramter is the angle for the FOV, the 2nd is the aspect ratio of your game, 
                    // the 3rd is the nearplane distance from the camera and the last paramter is the farplane distance from the camera.
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(fov), aspectrat, 1f, 20000.0f);
                }
                mesh.Draw(); // draw the current mesh using the effects.
            }
        }
    }

}