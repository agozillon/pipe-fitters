using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace standardclasses
{
    // Class for 2D graphics
    public class graphic2d
    {
        public Texture2D image;                 // Texture to hold image
        public Rectangle rect;                  // Rectangle to hold position & size of the image

        public graphic2d() { }


        // Constructor which fits image to screen resolution and keeps aspect ratio the same
        public graphic2d(ContentManager content, string spritename, int dwidth, int dheight)
        {
            image = content.Load<Texture2D>(spritename);    // Load 2D image into texture variable
            float aratio1 = ((float)dwidth / (float)dheight);             // Work out aspect ratio of the screen
            float aratio2 = ((float)image.Width / (float)image.Height);   // Work out aspect ratio of image
            if (aratio1 > aratio2)
            {
                float ratio = ((float)dwidth / image.Width);    // Work out the ratio for the image depending on screen size
                rect.Width = dwidth;                            // Set image width to match the screen width
                rect.Height = (int)(image.Height * ratio);      // Work out new height based on the screen aspect ratio
                rect.X = 0;
                rect.Y = (dheight - rect.Height) / 2;           // Put image in the middle of the screen on the Y axis
            }
            else
            {
                float ratio = ((float)dheight / image.Height);    // Work out the ratio for the image depending on screen size
                rect.Height = dheight;                          // Set image height to match the screen height
                rect.Width = (int)(image.Width * ratio);        // Work out new height based on the screen aspect ratio
                rect.Y = 0;
                rect.X = (dwidth - rect.Width) / 2;             // Put image in the middle of the screen on the X axis
            }
        }

        // Constructor which loads image and allows user to set x,y and the size ratio
        public graphic2d(ContentManager content, string spritename, int x, int y, float size)
        {
            image = content.Load<Texture2D>(spritename);
            rect.X = x;
            rect.Y = y;
            rect.Height = (int)(image.Height * size);
            rect.Width = (int)(image.Width * size);
        }

        // Use this method to draw the image
        public void drawme(ref SpriteBatch spriteBatch2)
        {
            spriteBatch2.Draw(image, rect, Color.White);
        }

    }

    // Class for 2D sprites
    public class sprite2d
    {
        public Texture2D image;         		// Texture which holds image
        public Vector3 position; 		 	    // Position on screen
        public Rectangle rect;          		// Rectangle to hold size and position
        public Vector2 origin;          		// Centre point
        public float size=1f;            		// Size ratio 
        public float rotation=0;          	    // Amount of rotation to apply
        public Vector3 velocity;        		// Velocity (Direction and speed)
        public Vector3 direction;       		// Vector direction
        public float thrust;                    // Forward or backwards force
        public float speed;             		// Actual speed
        public float rotationspeed;             // Speed that object can rotate at
        public BoundingSphere bsphere;  		// Bounding sphere
        public BoundingBox bbox;     		    // Bounding box
        public Vector3 oldposition;     		// Old position before collision occurs
        public Color colour = Color.White;	    // Colour of sprite
        public Boolean visible=true;    		// Should object be drawn true or false

        public sprite2d() { }

        // Constructor which fits image to screen resolution and keeps aspect ratio the same
        public sprite2d(ContentManager content, string spritename, int x, int y, float msize, Color mcolour, Boolean mvis)
        {
            image = content.Load<Texture2D>(spritename);    // Load image into texture
            position = new Vector3((float)x,(float)y,0);    // Set position
            size = msize;                                   // Size ratio
            origin.X = image.Width / 2;               	    // Set X origin to half of width
            origin.Y = image.Height / 2;              	    // Set Y origin to half of height
            rect.Width = (int)(image.Width * size);  	    // Set the new width based on the size ratio 
            rect.Height = (int)(image.Height * size);	    // Set the new height based on the size ratio
            rect.X = x;                                     // Set position of draw rectangle x
            rect.Y = y;                                     // Set position of draw rectangle y
            colour = mcolour;                               // Set colour
            visible = mvis;                                 // Ship visibility 
        }

        public void updatebox()
        {
            rect.X = (int)position.X;
            rect.Y = (int)position.Y;
            bbox = new BoundingBox(new Vector3(position.X - (rect.Width / 2), position.Y - (rect.Height / 2), position.Z), new Vector3(position.X + (rect.Width / 2), position.Y + (rect.Height / 2), position.Z));
        }

        public void updatesphere()
        {
            rect.X = (int)position.X;
            rect.Y = (int)position.Y;
            bsphere = new BoundingSphere(position, rect.Width / 2);
        }

        // Use this method to draw the image
        public void drawme(ref SpriteBatch spriteBatch2)
        {
            spriteBatch2.Draw(image, rect, null, colour, rotation, origin, SpriteEffects.None, 0);
        }
    }

    // Class for 3D graphics
    public class model3d
    {
        public Model graphic;           // The 3D Model object that we are going to display.
        public Matrix[] transforms;     // Holds model transformation matrix
        public Vector3 rotation;        // Amount of rotation to apply on x,y and z
        public Vector3 position;        // Position on screen
        public float size;              // Size ratio (scale) 
        public Vector3 velocity;        // Velocity (Direction and speed)
        public float radius;            // Radius of 3D model
        public BoundingSphere bsphere;  // Bounding sphere
        public Boolean visible;         // Should model be drawn?

        public Vector3 oldposition;     // Position before collision occurs
        public Vector3 direction;       // Direction object is pointing in
        public float thrust;            // Thrust or Speed
        public float weight;            // Weight of object

        public BoundingBox bbox;        // Bounding box
        public float height;            // Height of object

        public model3d() { }

        // Constructor which loads 3D model and sets it up with size, position and initial rotation
        public model3d(ContentManager content, string modelname, float msize, Vector3 mpos, Vector3 mrot)
        {
            graphic = content.Load<Model>(modelname);                   // Load the 3D model from the ContentManager
            transforms = new Matrix[graphic.Bones.Count];               // make an array of transforms, one for each 'bone' in the 3d model
            graphic.CopyAbsoluteBoneTransformsTo(transforms);           // copy the transforms from the 3d model into this array ready for use
            size = msize;                                               // Set size 
            radius = graphic.Meshes[0].BoundingSphere.Radius * size;    // Work out the radius 
            position = mpos;                                            // Set initial position
            rotation = mrot;                                            // Set intial rotation            
            visible = true;                                             // Set visible to true
        }
    }
}